extends Node2D


enum SWITCHSTATES {LEFT, RIGHT}
var current_state = SWITCHSTATES.LEFT

export (Vector2) var tile = Vector2(0,0)
export var left_tile = 7
export var right_tile = 7

onready var tilemap = get_node("../TileMap")

func _on_Area2D_area_entered(area):
	$AudioStreamPlayer.play()
	match current_state:
		SWITCHSTATES.LEFT:
			current_state = SWITCHSTATES.RIGHT
			$switchleft.hide()
			$switchright.show()
			tilemap.set_cellv(tile, right_tile)
		SWITCHSTATES.RIGHT:
			current_state = SWITCHSTATES.LEFT
			$switchleft.show()
			$switchright.hide()
			tilemap.set_cellv(tile, left_tile)

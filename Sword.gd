extends Area2D

var sword = true
var num = 0 #to make slashes distinguishable

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func activate():
	num += 1
	show()
	monitorable = true

func deactivate():
	hide()
	monitorable = false

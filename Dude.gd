extends KinematicBody2D

enum STATUS {STAND, WALK, JUMPIN}
enum DIRECTIONS {LEFT, RIGHT, UP, DOWN}

var is_dude = true

var flipped = false
var current_status = STATUS.STAND
var speed = 128
var startjump = Vector2()
var goalcart = Vector2()
var cart
var jumpdelta = 0
var jumplength = 0.2

export var rider = false

#direction
var current_look_direction = DIRECTIONS.RIGHT
#slashing
enum SLASHSTATES{IDLE, SLASH, COOLDOWN}
export var slash_length = 0.2
export var slash_cooldown = 0.1
var current_slash_state = SLASHSTATES.IDLE
var slash_delta = 0
var slash_dir = DIRECTIONS.UP

func _physics_process(delta):
	var direction = Vector2()
	if Input.is_action_pressed("ui_down"):
		direction.y += 1
	if Input.is_action_pressed("ui_up"):
		direction.y -= 1
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
	direction = direction.normalized()
	if direction.x > 0 and flipped:
		flipped = false
		$AnimatedSprite.flip_h = false
	if direction.x < 0 and not flipped:
		flipped = true
		$AnimatedSprite.flip_h = true
	match current_status:
		STATUS.STAND:
			swordslash(delta)
			var mov = move_and_slide(direction * speed)
			if mov != Vector2(0,0):
				current_status = STATUS.WALK
				$AnimatedSprite.play("walk")
		STATUS.WALK:
			swordslash(delta)
			var mov = move_and_slide(direction * speed)
			if mov == Vector2(0,0):
				current_status = STATUS.STAND
				$AnimatedSprite.play("stand")
				$AnimatedSprite.stop()
		STATUS.JUMPIN:
			jumpdelta += delta
			position = startjump.linear_interpolate(goalcart, jumpdelta/jumplength)
			if jumpdelta >= jumplength:
				cart.activate()
				queue_free()

func swordslash(delta):
		#direction
	if Input.is_action_just_pressed("ui_down"):
		current_look_direction = DIRECTIONS.DOWN
	if Input.is_action_just_pressed("ui_up"):
		current_look_direction = DIRECTIONS.UP
	if Input.is_action_just_pressed("ui_left"):
		current_look_direction = DIRECTIONS.LEFT
	if Input.is_action_just_pressed("ui_right"):
		current_look_direction = DIRECTIONS.RIGHT
	
	#slash
	match current_slash_state:
		SLASHSTATES.IDLE:
			if Input.is_action_just_pressed("ui_select"):
				$AudioStreamPlayerSlash.play()
				match current_look_direction:
					DIRECTIONS.UP:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.UP
						$SwordUp.activate()
					DIRECTIONS.DOWN:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.DOWN
						$SwordDown.activate()
					DIRECTIONS.RIGHT:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.RIGHT
						$SwordRight.activate()
					DIRECTIONS.LEFT:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.LEFT
						$SwordLeft.activate()
		SLASHSTATES.SLASH:
			slash_delta += delta
			if slash_delta >= slash_length:
				slash_delta = 0
				current_slash_state = SLASHSTATES.COOLDOWN
				$SwordDown.deactivate()
				$SwordUp.deactivate()
				$SwordLeft.deactivate()
				$SwordRight.deactivate()
		SLASHSTATES.COOLDOWN:
			slash_delta += delta
			if slash_delta >= slash_cooldown:
				slash_delta = 0
				current_slash_state = SLASHSTATES.IDLE

func _on_Area2D_area_entered(area):
	if "is_cart" in area and area.is_cart and rider:
		cart = area
		goalcart = area.position
		startjump = position
		$AnimatedSprite.stop()
		current_status = STATUS.JUMPIN
		$AudioStreamPlayerJump.play()


func _on_AnimatedSprite_frame_changed():
	$AudioStreamPlayer.play()

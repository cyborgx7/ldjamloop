extends Area2D

var is_cart = true

enum CARTSTATES {STOP, GO}
var current_state = CARTSTATES.STOP

#movement
enum DIRECTIONS{UP, DOWN, LEFT, RIGHT}
export var current_direction = DIRECTIONS.RIGHT
export var speed = 300
var current_tile

onready var tilemap = get_node("../TileMap")
var TILES = {-1:"Empty", 1:"CurveDR", 2:"StraightLR", 3:"StraightUD", 4:"CurveDL", 5:"CurveUR", 6:"CurveUL", 7:"Floor"}

#direction
var current_look_direction = DIRECTIONS.RIGHT
#slashing
enum SLASHSTATES{IDLE, SLASH, COOLDOWN}
export var slash_length = 0.2
export var slash_cooldown = 0.1
var current_slash_state = SLASHSTATES.IDLE
var slash_delta = 0
var slash_dir = DIRECTIONS.UP


# Called when the node enters the scene tree for the first time.
func _ready():
	current_tile = tilemap.world_to_map(position)
	pass

func _process(delta):
	match current_state:
		CARTSTATES.STOP:
			if Input.is_action_just_pressed("startcart"):
				activate()
		CARTSTATES.GO:
			move(delta)

func move(delta):
	#cart movement
	match current_direction:
		DIRECTIONS.UP:
			position.y -= delta*speed
			if position.y <= tilemap.map_to_world(current_tile).y+32:
				next_tile()
		DIRECTIONS.DOWN:
			position.y += delta*speed
			if position.y >= tilemap.map_to_world(current_tile).y+32:
				next_tile()
		DIRECTIONS.RIGHT:
			position.x += delta*speed
			if position.x >= tilemap.map_to_world(current_tile).x+32:
				next_tile()
		DIRECTIONS.LEFT:
			position.x -= delta*speed
			if position.x <= tilemap.map_to_world(current_tile).x+32:
				next_tile()
	
	#direction
	if Input.is_action_just_pressed("ui_down"):
		current_look_direction = DIRECTIONS.DOWN
		$RiderSprites.play("down")
	if Input.is_action_just_pressed("ui_up"):
		current_look_direction = DIRECTIONS.UP
		$RiderSprites.play("up")
	if Input.is_action_just_pressed("ui_left"):
		current_look_direction = DIRECTIONS.LEFT
		$RiderSprites.play("left")
	if Input.is_action_just_pressed("ui_right"):
		current_look_direction = DIRECTIONS.RIGHT
		$RiderSprites.play("right")
	
	#slash
	match current_slash_state:
		SLASHSTATES.IDLE:
			if Input.is_action_just_pressed("ui_select"):
				$AudioStreamPlayerSlash.play()
				match current_look_direction:
					DIRECTIONS.UP:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.UP
						$SwordUp.activate()
					DIRECTIONS.DOWN:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.DOWN
						$SwordDown.activate()
					DIRECTIONS.RIGHT:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.RIGHT
						$SwordRight.activate()
					DIRECTIONS.LEFT:
						current_slash_state = SLASHSTATES.SLASH
						slash_dir = DIRECTIONS.LEFT
						$SwordLeft.activate()
		SLASHSTATES.SLASH:
			slash_delta += delta
			if slash_delta >= slash_length:
				slash_delta = 0
				current_slash_state = SLASHSTATES.COOLDOWN
				$SwordDown.deactivate()
				$SwordUp.deactivate()
				$SwordLeft.deactivate()
				$SwordRight.deactivate()
		SLASHSTATES.COOLDOWN:
			slash_delta += delta
			if slash_delta >= slash_cooldown:
				slash_delta = 0
				current_slash_state = SLASHSTATES.IDLE

func next_tile():
	#update direction
	match TILES[tilemap.get_cellv(current_tile)]:
		"CurveDR":
			match current_direction:
				DIRECTIONS.UP:
					current_direction = DIRECTIONS.RIGHT
					$Cartsprites.play("side")
				DIRECTIONS.DOWN:
					pass
				DIRECTIONS.LEFT:
					current_direction = DIRECTIONS.DOWN
					$Cartsprites.play("front")
				DIRECTIONS.RIGHT:
					pass
		"CurveDL":
			match current_direction:
				DIRECTIONS.UP:
					current_direction = DIRECTIONS.LEFT
					$Cartsprites.play("side")
				DIRECTIONS.DOWN:
					pass
				DIRECTIONS.LEFT:
					pass
				DIRECTIONS.RIGHT:
					current_direction = DIRECTIONS.DOWN
					$Cartsprites.play("front")
		"CurveUL":
			match current_direction:
				DIRECTIONS.UP:
					pass
				DIRECTIONS.DOWN:
					current_direction = DIRECTIONS.LEFT
					$Cartsprites.play("side")
				DIRECTIONS.LEFT:
					pass
				DIRECTIONS.RIGHT:
					current_direction = DIRECTIONS.UP
					$Cartsprites.play("front")
		"CurveUR":
			match current_direction:
				DIRECTIONS.UP:
					pass
				DIRECTIONS.DOWN:
					current_direction = DIRECTIONS.RIGHT
					$Cartsprites.play("side")
				DIRECTIONS.LEFT:
					current_direction = DIRECTIONS.UP
					$Cartsprites.play("front")
				DIRECTIONS.RIGHT:
					pass
	#update_tile
	match current_direction:
		DIRECTIONS.UP:
			current_tile.y -= 1
		DIRECTIONS.DOWN:
			current_tile.y +=1
		DIRECTIONS.LEFT:
			current_tile.x -= 1
		DIRECTIONS.RIGHT:
			current_tile.x += 1

func activate():
	current_state = CARTSTATES.GO
	$RiderSprites.show()
	$AudioStreamPlayer.play()

func deactivate():
	current_state = CARTSTATES.STOP
	$RiderSprites.hide()
	$AudioStreamPlayer.stop()

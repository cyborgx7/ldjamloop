extends Area2D

signal move_camera(position)

func _on_Cameramover_area_entered(area):
	if "is_cart" in area and area.is_cart:
		emit_signal("move_camera", $GoalPos.global_position)

extends Camera2D

enum CAMERASTATUSES {IDLE, MOVING}
var current_state = CAMERASTATUSES.IDLE
export var movement_time = 0.5
var movement_delta = 0
onready var last_pos = global_position
var goal_pos = Vector2()

func _process(delta):
	match current_state:
		CAMERASTATUSES.IDLE:
			pass
		CAMERASTATUSES.MOVING:
			movement_delta = min(movement_delta+delta, movement_time)
			global_position = last_pos.linear_interpolate(goal_pos, movement_delta/movement_time)
			if movement_delta >= movement_time:
				last_pos = global_position
				current_state = CAMERASTATUSES.IDLE
				movement_delta = 0

func _on_Cameramover_move_camera(pos):
	print("new goal")
	goal_pos = pos
	current_state = CAMERASTATUSES.MOVING

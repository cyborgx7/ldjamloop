extends Area2D

const DUDE = preload("res://Dude.tscn")

func _on_Stoppage_area_entered(area):
	if "is_cart" in area and area.is_cart:
		area.deactivate()
		var dude = DUDE.instance()
		get_parent().add_child(dude)
		dude.global_position = $Position2D.global_position
		
